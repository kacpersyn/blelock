#include "cPIN.h"
#include "mbed.h"

void writeReg(unsigned addr, uint32_t data)
{
    *((unsigned *) addr) = data;
    wait_ms(100);
}

uint32_t readReg(unsigned addr)
{
    return ((uint32_t) *((unsigned *) addr));
}

bool enableWrite(void )
{
    writeReg(NVMC_CONFIG_EXCT_ADDR, NVM_WRITE_EN);
    return (readReg(NVMC_CONFIG_EXCT_ADDR) == (uint32_t) NVM_WRITE_EN ? true : false);
}

bool disableWrite(void )
{
    writeReg(NVMC_CONFIG_EXCT_ADDR, NVM_READ_ONLY);
    return (readReg(NVMC_CONFIG_EXCT_ADDR) == (uint32_t) NVM_READ_ONLY ? true : false);
}

bool enableErase(void )
{
    writeReg(NVMC_CONFIG_EXCT_ADDR, NVM_ERASE_EN);
    return (readReg(NVMC_CONFIG_EXCT_ADDR) == (uint32_t) NVM_ERASE_EN ? true : false);
}

bool disableErase(void )
{
    writeReg(NVMC_CONFIG_EXCT_ADDR, NVM_READ_ONLY);
    return (readReg(NVMC_CONFIG_EXCT_ADDR) == (uint32_t) NVM_READ_ONLY ? true : false);
}

bool writePINCode(uint32_t PINCode, uint8_t RegisterNumber)
{
    bool success;

    if (RegisterNumber > CUSTOMER_REG_COUNT-1) 
        return false;
    else
    {
        success = enableWrite();
        writeReg(UICR_BASE_ADDR+CUSTOMER_OFFSET_ADDR+0x4*RegisterNumber, PINCode);
        wait_ms(500);
        success &= disableWrite();
    }
    
    return (((readPINCode(RegisterNumber) == PINCode) && success) ? true : false);
}

uint32_t readPINCode(uint32_t RegisterNumber)
{
    return readReg(UICR_BASE_ADDR+CUSTOMER_OFFSET_ADDR+0x4*RegisterNumber);
}

void eraseFlash(void )
{
    writeReg(NVMC_CONFIG_EXCT_ADDR, NVM_ERASE_EN);
    writeReg(NVM_ERASE_UICR_EXCT_ADDR, NVM_UICR_ERASE);
    wait_ms(1000);
    writeReg(NVM_ERASE_UICR_EXCT_ADDR, NVM_UICR_NOP);
    writeReg(NVMC_CONFIG_EXCT_ADDR, NVM_READ_ONLY);
}

bool iWantToBeSure(void )
{
    return (readReg(NVMC_CONFIG_EXCT_ADDR) == (uint32_t) NVM_READ_ONLY);
}

bool changeUSRPINCode(uint32_t new_pincode)
{
    bool success = true;
    printf("00, okej");
    uint32_t temp[CUSTOMER_REG_COUNT];
    for(int i=0; i<CUSTOMER_REG_COUNT; i++)
    {
        temp[i] = readPINCode(i);
        printf("%d\n\r",i);
    }
    printf("01, okej");
    eraseFlash();
    printf("02, okej");
    success &= enableWrite();
    printf("03, okej");
    for(int i=0; i<CUSTOMER_REG_COUNT; i++)
    {
        if(i != temp[0])
        {
            writeReg(UICR_BASE_ADDR+CUSTOMER_OFFSET_ADDR+0x4*i, temp[i]);
            wait_ms(150);
            success &= (readPINCode(i)==temp[i]); 
        }
        else
        {
            writeReg(UICR_BASE_ADDR+CUSTOMER_OFFSET_ADDR+0x4*temp[0], new_pincode);
            wait_ms(150);
            success &= (readPINCode(temp[0])==new_pincode);
        }
    }

    success &= disableWrite();
    success &= iWantToBeSure();

    return success;
}

bool changeADMPINCode(uint32_t new_pincode)
{
    bool success = true;

    uint32_t temp[CUSTOMER_REG_COUNT];
    for(int i=0; i<CUSTOMER_REG_COUNT; i++)
    {
        temp[i] = readPINCode(i);
    }

    eraseFlash();
        
    success &= enableWrite();
    for(int i=0; i<CUSTOMER_REG_COUNT; i++)
    {
        if(i != temp[16])
        {
            writeReg(UICR_BASE_ADDR+CUSTOMER_OFFSET_ADDR+0x4*i, temp[i]);
            wait_ms(150);
            success &= (readPINCode(i)==temp[i]); 
        }
        else
        {
            writeReg(UICR_BASE_ADDR+CUSTOMER_OFFSET_ADDR+0x4*temp[16], new_pincode);
            wait_ms(150);
            success &= (readPINCode(temp[16])==new_pincode);
        }
    }

    success &= disableWrite();
    success &= iWantToBeSure();

    return success;
}