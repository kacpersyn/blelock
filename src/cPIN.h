#ifndef PIN_H
#define PIN_H

#include "stdint.h"

#define NVMC_CONFIG_EXCT_ADDR 0x4001E504
#define NVM_READ_ONLY 0x0
#define NVM_WRITE_EN 0x1
#define NVM_ERASE_EN 0x2

#define NVM_ERASE_UICR_EXCT_ADDR 0x4001E514
#define NVM_UICR_ERASE 0x1
#define NVM_UICR_NOP 0x0

#define UICR_BASE_ADDR 0x10001000
#define CUSTOMER_OFFSET_ADDR 0x80
#define CUSTOMER_REG_COUNT 32

bool changeUSRPINCode(uint32_t new_pincode);
bool changeADMPINCode(uint32_t new_pincode);

bool writePINCode(uint32_t pincode, uint8_t registerNumber);    // registerNumber = [0,31]
uint32_t readPINCode(uint32_t registerNumber);
bool iWantToBeSure(void );
void eraseFlash(void );

void writeReg(unsigned addr, uint32_t data);
uint32_t readReg(unsigned addr);
bool enableWrite(void );
bool disableWrite(void );
bool enableErase(void );
bool disableErase(void );

#endif
