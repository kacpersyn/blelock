#include "mbed.h"
#include "ble/BLE.h"
#include "cPIN.h"
#include "fds.h"

#define ID_STORAGE_USR 0
#define ID_STORAGE_ADM 16

#define TIME_1 15
#define TIME_2 (5*60)

#define DEFAULT_ADMIN_PIN 4321
#define DEFAULT_USER_PIN 1234

// Return codes definitions
#define NOP 0xFFFFFFFF
#define ADM_LOG (0x1)
#define USR_LOG (0x1<<1)

#define DOOR_CLOSED (0x1<<2)
#define DOOR_TIM1_OPEN (0x1<<3)
#define DOOR_TIM2_OPEN (0x1<<4)

#define PIN_ADM_CHANGED (0x1<<5)
#define PIN_USR_CHANGED (0x1<<6)

#define FILE_ID         0x0001
#define RECORD_KEY_1    0x1111
#define RECORD_KEY_2    0x2222
#define TIMEOUT         180

// Flags
static bool isUserLoggedIn = false;
static bool isAdminLoggedIn = false;
static bool isDoorOpen1 = false; // Englessi perfecto
static bool isDoorOpen2 = false;
static bool isAPRequest = false;
static bool isUPRequest = false;
static bool isUSRPINCodeChanged = false;
static bool isADMPINCodeChanged = false;
static bool disconnectBLE = false;
static bool isConnected = false;
static bool isCloseLockTimerAttached = false;
static uint8_t outputMessage[4];
static uint32_t UserPINToBeChanged = 1211;

// PINCode data
static volatile uint32_t USRPINCODE = 0;
static volatile uint8_t USRStorageID = 0;
static volatile uint32_t ADMPINCODE = 0;
static volatile uint8_t ADMStorageID = 0;

Timeout closeLockTimer;
Timeout disconnectTimeout;

void SetReturnMessage(uint8_t *message)
{
    if (isAdminLoggedIn)
        message[0] |= ADM_LOG;
    if (isUserLoggedIn)
        message[0] |= USR_LOG;
    if (isDoorOpen1)
        message[0] |= DOOR_TIM1_OPEN;
    if (isDoorOpen2)
        message[0] |= DOOR_TIM2_OPEN;
    if (isADMPINCodeChanged)
        message[0] |= PIN_ADM_CHANGED;
    if (isUSRPINCodeChanged)
        message[0] |= PIN_USR_CHANGED;
}

void ClearReturnMessage(uint8_t *message)
{
    message[0] = (uint8_t) 0;
}

DigitalOut led1(LED1, 1);
DigitalOut led2(LED2, 1);
DigitalOut led3(LED3, 1);
DigitalOut led4(LED4, 1);
DigitalOut lockPin(D5, 0);

uint16_t customServiceUUID  = 0xA000;
uint16_t readCharUUID       = 0xA001;
uint16_t logInUUID          = 0xA002;
uint16_t openLockUUID       = 0xA003;
uint16_t writeNewPINUUID    = 0xA004;
uint16_t closeLockUUID      = 0xA005;
 
const static char     DEVICE_NAME[]        = "BlelOK"; // change this
static const uint16_t uuid16_list[]        = {0xFFFF}; //Custom UUID, FFFF is reserved for development

GattAttribute readCharDescr(BLE_UUID_DESCRIPTOR_CHAR_USER_DESC, (uint8_t *)"readChar", strlen("readChar"));
GattAttribute logInDescr( BLE_UUID_DESCRIPTOR_CHAR_USER_DESC, (uint8_t *)"logIn", strlen("logIn"));
GattAttribute openLockDescr( BLE_UUID_DESCRIPTOR_CHAR_USER_DESC, (uint8_t *)"openLock", strlen("openLock"));
GattAttribute writeNewPINDescr( BLE_UUID_DESCRIPTOR_CHAR_USER_DESC, (uint8_t *)"writeNewPIN", strlen("writeNewPIN"));
GattAttribute closeLockDescr( BLE_UUID_DESCRIPTOR_CHAR_USER_DESC, (uint8_t *)"closeLock", strlen("closeLock"));

GattAttribute *readCharDescrTable[] = {&readCharDescr};
GattAttribute *logInDescrTable[] = {&logInDescr};
GattAttribute *openLockDescrTable[] = {&openLockDescr};
GattAttribute *writeNewPINDescrTable[] = {&writeNewPINDescr};
GattAttribute *closeLockDescrTable[] = {&closeLockDescr};

static uint8_t readValue[10] = {0};
ReadOnlyArrayGattCharacteristic<uint8_t, sizeof(readValue)> readChar(readCharUUID, readValue, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_EXTENDED_PROPERTIES, readCharDescrTable, 
                                                                    sizeof(readCharDescrTable) / sizeof(GattAttribute*));
 
static uint8_t writeValue[10] = {0};
WriteOnlyArrayGattCharacteristic<uint8_t, sizeof(writeValue)> logIn(logInUUID, writeValue, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_EXTENDED_PROPERTIES, logInDescrTable, 
                                                                    sizeof(logInDescrTable) / sizeof(GattAttribute*));
static uint8_t lockValue[2] = {0}; //the opening time of the lock
WriteOnlyArrayGattCharacteristic<uint8_t, sizeof(lockValue)> openLock(openLockUUID, lockValue, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_EXTENDED_PROPERTIES, openLockDescrTable, 
                                                                    sizeof(openLockDescrTable) / sizeof(GattAttribute*));

static uint8_t writeNewPINl[10] = {0};
WriteOnlyArrayGattCharacteristic<uint8_t, sizeof(writeNewPINl)> writeNewPIN(writeNewPINUUID, writeNewPINl, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_EXTENDED_PROPERTIES, writeNewPINDescrTable, 
                                                                    sizeof(writeNewPINDescrTable) / sizeof(GattAttribute*));
static uint8_t closeLockValue[1] = {0};
WriteOnlyArrayGattCharacteristic<uint8_t, sizeof(closeLockValue)> closeLock(closeLockUUID, closeLockValue, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_EXTENDED_PROPERTIES, closeLockDescrTable, 
                                                                    sizeof(closeLockDescrTable) / sizeof(GattAttribute*));
GattCharacteristic *characteristics[] = {&readChar, &logIn, &openLock, &writeNewPIN, &closeLock};
GattService        customService(customServiceUUID, characteristics, sizeof(characteristics) / sizeof(GattCharacteristic *));

void closeLockCallback()
{
    closeLockTimer.detach();
    isCloseLockTimerAttached = false;
    isDoorOpen1 = false;
    isDoorOpen2 = false;
}

void disconnectTimeoutCallback()
{
    disconnectTimeout.detach();
    disconnectBLE = true;
}

void disconnectionCallback(const Gap::DisconnectionCallbackParams_t *)
{
    isConnected = false;
    isUserLoggedIn = false;
    isAdminLoggedIn = false;
    isUSRPINCodeChanged = false;
    isADMPINCodeChanged = false;
    ClearReturnMessage(outputMessage);
    BLE::Instance(BLE::DEFAULT_INSTANCE).gap().startAdvertising();
}
void connectionCallback(const Gap::ConnectionCallbackParams_t *)
{
    isConnected = true;
    disconnectTimeout.attach(&disconnectTimeoutCallback, TIMEOUT);
}
void writeCharCallback(const GattWriteCallbackParams *params)
{
    if(params->handle == logIn.getValueHandle()) 
    {
        if(params->len == 4) // WPISYWANIE PINU, 4 BAJTOWEGO
        {
            uint32_t *ptr = (uint32_t  *) params->data;
            if(*ptr == USRPINCODE)  // WPISANO PIN UZYTKOWNIKA
            {
                isUserLoggedIn = true;
            }
            if(*ptr == ADMPINCODE)  // WPISANO PIN ADMINA
            {
                isAdminLoggedIn = true;
            }
        }
        
    }
    else if(params->handle == openLock.getValueHandle())
    {

        if((isUserLoggedIn || isAdminLoggedIn) && !(isDoorOpen1 || isDoorOpen2))
        {
            uint16_t *timeInSeconds= (uint16_t *) params->data;
            if(*timeInSeconds < TIME_2)
            {
                isDoorOpen1 = true;
            }
            else
            {
                isDoorOpen2 = true;
            }
            closeLockTimer.attach(&closeLockCallback, *timeInSeconds);
            isCloseLockTimerAttached = true;
        }
    
    }
    else if(params->handle == writeNewPIN.getValueHandle())
    {
        if(params->len == 4) // WPISYWANIE PINU, 4 BAJTOWEGO
        {
            uint32_t *ptr = (uint32_t  *) params->data;
            if(isAdminLoggedIn)
            {   
                isUPRequest = true;
                UserPINToBeChanged = *ptr;
            }
        }
    }
    else if(params->handle == closeLock.getValueHandle())
    {
        if(params->len == 1)
        {
            uint8_t *ptr = (uint8_t  *) params->data;
            if(*ptr == 1 && (isAdminLoggedIn || isUserLoggedIn))
            {   
                if(isCloseLockTimerAttached)
                {
                    closeLockTimer.detach();
                    isCloseLockTimerAttached = false;
                }
                isDoorOpen1 = false;
                isDoorOpen2 = false;
            }
        }
    }
}

void bleInitComplete(BLE::InitializationCompleteCallbackContext *params)
{
    BLE &ble          = params->ble;
    ble_error_t error = params->error;
    
    if (error != BLE_ERROR_NONE) {
        return;
    }
 
    ble.gap().onConnection(connectionCallback);
    ble.gap().onDisconnection(disconnectionCallback);
    ble.gattServer().onDataWritten(writeCharCallback);
 
    /* Setup advertising */
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED | GapAdvertisingData::LE_GENERAL_DISCOVERABLE); // BLE only, no classic BT
    ble.gap().setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED); // advertising type
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LOCAL_NAME, (uint8_t *)DEVICE_NAME, sizeof(DEVICE_NAME)); // add name
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LIST_16BIT_SERVICE_IDS, (uint8_t *)uuid16_list, sizeof(uuid16_list)); // UUID's broadcast in advertising packet
    ble.gap().setAdvertisingInterval(100); // 100ms.
 
    /* Add our custom service */
    ble.addService(customService);
 
    /* Start advertising */
    ble.gap().startAdvertising();
}
 
int main(void)
{
    bool success;

    uint32_t adminRecordIterator = 1;
    uint32_t userRecordIterator = 1;

    uint32_t *buffor;
    
    uint32_t defaultPINCode;
    defaultPINCode = DEFAULT_ADMIN_PIN;

    fds_init();
    wait_ms(100);

    ret_code_t rc;
    fds_record_t        record;
    fds_record_desc_t   record_desc;
    fds_flash_record_t  flash_record;
    fds_find_token_t    ftok;
    
    record.file_id           = FILE_ID;
    record.key               = RECORD_KEY_1;
    record.data.p_data       = &defaultPINCode;
    record.data.length_words = 1;
    //rc = fds_record_write(&record_desc, &record); // Uncomment to write default password

    defaultPINCode = DEFAULT_USER_PIN;
    record.key               = RECORD_KEY_2;
    record.data.p_data       = &defaultPINCode;
    //rc = fds_record_write(&record_desc, &record); // Uncomment to write default password

    BLE& ble = BLE::Instance(BLE::DEFAULT_INSTANCE);
    ble.init(bleInitComplete);
    
    while (ble.hasInitialized()  == false) {}

    // Get passwords from flash
    memset(&ftok, 0x00, sizeof(fds_find_token_t));

    int pinIterator = 0;
    while (fds_record_find(FILE_ID, RECORD_KEY_1, &record_desc, &ftok) == FDS_SUCCESS)
    {
        if (fds_record_open(&record_desc, &flash_record) != FDS_SUCCESS)
        {
            printf("Flash-open failed\n\r");
        }

        buffor = (uint32_t *) flash_record.p_data;
        ADMPINCODE = *buffor;

        if (fds_record_close(&record_desc) != FDS_SUCCESS)
        {
            printf("Flash-close failed\n\r");
        }

        if (pinIterator < adminRecordIterator)
            break;
        pinIterator++;
    }

    printf("Admin pincode retrieved\n\r");

    pinIterator = 0;
    while (fds_record_find(FILE_ID, RECORD_KEY_2, &record_desc, &ftok) == FDS_SUCCESS)
    {
        if (fds_record_open(&record_desc, &flash_record) != FDS_SUCCESS)
        {
            printf("Flash-open failed\n\r");
        }
        
        buffor = (uint32_t *) flash_record.p_data;
        USRPINCODE = *buffor;

        if (fds_record_close(&record_desc) != FDS_SUCCESS)
        {
            printf("Flash-close failed\n\r");
        }

        if (pinIterator < adminRecordIterator)
            break;
        pinIterator++;
    }
    
    //debug
    printf("USR ID: %d, %d\n\r", USRStorageID, USRPINCODE);
    printf("ADM ID: %d, %d\n\r", ADMStorageID, ADMPINCODE);

    /* Infinite loop waiting for BLE interrupt events */
    while (true) {
        if(isUPRequest && isAdminLoggedIn)
        {
            printf("1: %d\n\r", UserPINToBeChanged);

            rc = fds_record_delete(&record_desc);
            if (rc != FDS_SUCCESS)
                printf("Record deleting was faulty\n\r");

            wait_ms(500);

            record.key               = RECORD_KEY_2;
            record.data.p_data       = &UserPINToBeChanged;
            rc = fds_record_write(&record_desc, &record); 
            if (rc != FDS_SUCCESS)
                printf("Writing new pincode failed\n\r");

            memset(&ftok, 0x00, sizeof(fds_find_token_t));
            pinIterator = 0;
            while (fds_record_find(FILE_ID, RECORD_KEY_2, &record_desc, &ftok) == FDS_SUCCESS)
            {
                if (fds_record_open(&record_desc, &flash_record) != FDS_SUCCESS)
                {
                    printf("Flash-open failed\n\r");
                }
        
                buffor = (uint32_t *) flash_record.p_data;
                USRPINCODE = *buffor;
                printf("NOWY PIN TO: %d", *buffor);
                
                if (fds_record_close(&record_desc) != FDS_SUCCESS)
                {
                    printf("Flash-close failed\n\r");
                }

                if (pinIterator < adminRecordIterator)
                    break;
                pinIterator++;
            }
            isUPRequest = false;
            isUSRPINCodeChanged = true;
        }

        ClearReturnMessage(outputMessage);
        SetReturnMessage(outputMessage);
        BLE::Instance(BLE::DEFAULT_INSTANCE).gattServer().write(
                    readChar.getValueHandle(), outputMessage, 4);


        //rc = fds_record_delete(&record_desc);
        pinIterator = 0;
        while (fds_record_find(FILE_ID, RECORD_KEY_2, &record_desc, &ftok) == FDS_SUCCESS)
        {
                if (fds_record_open(&record_desc, &flash_record) != FDS_SUCCESS)
            {
                printf("Flash-open failed\n\r");
            }
        
            buffor = (uint32_t *) flash_record.p_data;

            if (fds_record_close(&record_desc) != FDS_SUCCESS)
            {
                printf("Flash-close failed\n\r");
            }

            if (pinIterator < adminRecordIterator)
                break;
            pinIterator++;
        }

        led4 = !isUserLoggedIn;
        led3 = !isAdminLoggedIn;
        led2 = !isDoorOpen1;
        led1 = !isConnected;
        lockPin = isDoorOpen1 || isDoorOpen2;

        ble.waitForEvent();
        
        if(disconnectBLE == true)
        {
            disconnectBLE = false;
            Gap::DisconnectionReason_t res = Gap::LOCAL_HOST_TERMINATED_CONNECTION;
            ble.gap().disconnect(res);
        }


    }
}